#include "horizgrid.c"
/* Constants */
#define TERMINAL "st"
#define TERMCLASS "St"
#define NOTITLE 0 // if you like no title,  set it to 1
#define NOFANCYBAR                                                             \
  1 /* if you like to view all titles, set it to 0, if you like dwm native     \
       bar, leave it to 1 */

static const unsigned int borderpx = 3; /* border pixel of windows */
static const unsigned int snap = 30;    /* snap pixel */
static const unsigned int systraypinning =
    1; /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor
          X */
static const unsigned int systrayspacing = 2; /* systray spacing */
static const int systraypinningfailfirst =
    1; /* 1: if pinning fails, display systray on the first monitor, False:
          display systray on the last monitor*/
static const int showsystray = 1;      /* 0 means no systray */
static const unsigned int gappih = 30; /* horiz inner gap between windows */
static const unsigned int gappiv = 20; /* vert inner gap between windows */
static const unsigned int gappoh =
    20; /* horiz outer gap between windows and screen edge */
static const unsigned int gappov =
    30; /* vert outer gap between windows and screen edge */
static const int swallowfloating =
    1; /* 1 means swallow floating windows by default */
static const int smartgaps =
    0; /* 1 means no outer gap when there is only one window */
static const int showbar = 1; /* 0 means no bar */
static const int topbar = 1;  /* 0 means bottom bar */
static const int focusonwheel = 0;
static const int usealtbar = 0;             /* 1 means use non-dwm status bar */
static const char *altbarclass = "Polybar"; /* Alternate bar class name */
static const char *alttrayname = "tray";    /* Polybar tray instance name */
static const char *altbarcmd = "";
static const int startontag = 1;            /* 0 means no tag active on start */
static const char *fonts[] = {
    "FiraCode Nerd Font:size=12",
    "JoyPixels:pixelsize=12:antialias=true:autohint=true",
    "Microsoft YaHei:size=12:antialias=true:autohint=true"};
static char dmenufont[] = "FiraCode Nerd Font:size=12";
static char normbgcolor[] = "#333333";
static char normbordercolor[] = "#111111";
static char normfgcolor[] = "#FFFFCC";
static char selfgcolor[] = "#000000";
static char selbordercolor[] = "#800000";
static char selbgcolor[] = "#006699";
static char titlefgcolor[] = "#000000";
static char titlebgcolor[] = "#006699";
static char titleborcolor[] = "#800000";
static char *colors[][3] = {
    /*               fg           bg           border   */
    [SchemeNorm] = {normfgcolor, normbgcolor, normbordercolor},
    [SchemeSel] = {titlefgcolor, titlebgcolor, titleborcolor},
    [SchemeTitle] = {selfgcolor, selbgcolor, selbordercolor},
};

static const char *const autostart[] = {
    "dunst", NULL, NULL /* terminate */
};

typedef struct {
  const char *name;
  const void *cmd;
} Sp;
const char *spcmd1[] = {TERMINAL, "-n", "spterm", "-g", "120x34", NULL};
const char *spcmd2[] = {TERMINAL, "-n",    "spcalc", "-f", "monospace:size=12",
                        "-g",     "50x20", "-e",     "bc", "-lq",
                        NULL};
const char *spcmd3[] = {TERMINAL, "-n",    "sptrans", "-g", "120x40",
                        "-e",     "trans", "en:zh",   NULL};
const char *spcmd4[] = {TERMINAL, "-n", "spmusicbox", "-g",
                        "110x30", "-e", "musicbox",   NULL};
static Sp scratchpads[] = {
    /* name          cmd  */
    {"spterm", spcmd1},
    {"spranger", spcmd2},
    {"sptrans", spcmd3},
    {"spmusicbox", spcmd4},
};
/*home terminal wechar */

static const char *tags[] = {"", "", "", " ", "", "", "", "", ""};
static const char *tagsalt[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};


/* launcher commands (They must be NULL terminated) */
static const char *rofi[] = {"rofi", "-show", "drun", NULL};
static const char *browser[] = {"brave-bin", NULL};

static const Launcher launchers[] = {
    /* command       name to display */
    {rofi, ""},
    {browser, ""}};

/* 设置窗口状态 */
static const Rule rules[] = {
    /* xprop(1):
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING) = title
     */
    /* class    instance      title       	 tags mask    isfloating
       isterminal  noswallow  monitor */
    {TERMCLASS, NULL, NULL, 0, 0, 1, 0, -1},
    {"emacs", "Emacs", NULL, 1 << 2, 0, 0, 0, -1},
    {"brave-browser", "Brave-browser", NULL, 5, 0, 0, 0, -1},
    {NULL, NULL, "Event Tester", 0, 0, 0, 1, -1},
    {NULL, "spterm", NULL, SPTAG(0), 1, 1, 0, -1},
    {NULL, "spcalc", NULL, SPTAG(1), 1, 1, 0, -1},
    {NULL, "sptrans", NULL, SPTAG(2), 1, 1, 0, -1},
};
/* layout(s) */
static const float mfact = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster = 1;    /* number of clients in master area */
static const int resizehints =
    1; /* 1 means respect size hints in tiled resizals */
#define FORCE_VSPLIT                                                           \
  1 /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"
static const int scrollsensetivity =
    30; /* 1 means resize window by 1 pixel for each scroll event */
#include "layouts.c"
static const Layout layouts[] = {
    /* symbol     arrange function */
    {"[]=", tile}, /* Default: Master on left, slaves on right */
    /* {"Title", tile},   /1* Default: Master on left, slaves on right *1/ */
    {"TTT", bstack}, /* Master on top, slaves on bottom */
    /* {"Bstack", bstack}, /1* Master on top, slaves on bottom *1/ */

    {"[@]", spiral}, /* Fibonacci spiral */
    /* {"spiral", spiral},   /1* Fibonacci spiral *1/ */
    {"###", horizgrid},
    /* {"horizgrid", horizgrid}, */
    {"[\\]", dwindle}, /* Decreasing in size right and leftward */
    /* {"Dwindle", dwindle}, /1* Decreasing in size right and leftward *1/ */

    {"H[]", deck}, /* Master on left, slaves in monocle-like mode on right */
    /* {"Deck", deck},    /1* Master on left, slaves in monocle-like mode on
       right *1/ */
    {"[M]", monocle}, /* All windows on top of eachother */
    /* {"Monocle", monocle}, /1* All windows on top of eachother *1/ */

    {"|M|", centeredmaster}, /* Master in middle, slaves on sides */
    /* {"Centeredmaster", centeredmaster},         /1* Master in middle, slaves
       on sides *1/ */
    {">M>", centeredfloatingmaster}, /* Same but master floats */

    //{"><>", NULL}, /* no layout function means floating behavior */
    {"Floating", NULL}, /* no layout function means floating behavior */
    {"HHH", grid},
    {NULL, NULL},
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY, TAG)                                                      \
  {MODKEY, KEY, view, {.ui = 1 << TAG}},                                       \
      {MODKEY | ControlMask, KEY, toggleview, {.ui = 1 << TAG}},               \
      {MODKEY | ShiftMask, KEY, tag, {.ui = 1 << TAG}},                        \
      {MODKEY | ControlMask | ShiftMask, KEY, toggletag, {.ui = 1 << TAG}},
#define STACKKEYS(MOD, ACTION)                                                 \
  {MOD, XK_j, ACTION##stack, {.i = INC(+1)}},                                  \
      {MOD, XK_k, ACTION##stack, {.i = INC(-1)}},                              \
      {MOD,                                                                    \
       XK_v,                                                                   \
       ACTION##stack,                                                          \
       {.i = 0}}, /* { MOD, XK_grave, ACTION##stack, {.i = PREVSEL } }, \ */
/* { MOD, XK_a,     ACTION##stack, {.i = 1 } }, \ */
/* { MOD, XK_z,     ACTION##stack, {.i = 2 } }, \ */
/* { MOD, XK_x,     ACTION##stack, {.i = -1 } }, */

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd)                                                             \
  {                                                                            \
    .v = (const char *[]) { "/bin/sh", "-c", cmd, NULL }                       \
  }
#define ALT Mod1Mask
/* commands */
static char dmenumon[2] =
    "0"; /* component of dmenucmd, manipulated in nspawn() */
// static const char *dmenucmd[] = {"dmenu_run", "-g", "10", "-l", "10"};
static const char *dmenucmd[] = {"dmenu_run", "-fn", dmenufont, NULL};
static const char *termcmd[] = {TERMINAL, NULL};

#include "shiftview.c"
#include <X11/XF86keysym.h>
static Key keys[] = {
    /* modifier                     key        function        argument */
    STACKKEYS(MODKEY, focus) STACKKEYS(MODKEY | ShiftMask, push)
    /* { MODKEY|ShiftMask,		XK_Escape,	spawn,	SHCMD("") }, */
    {MODKEY, XK_grave, spawn, SHCMD("dmenuunicode")},
    /* { MODKEY|ShiftMask,		XK_grave,	togglescratch,
     * SHCMD("")
     * },
     */

    {MODKEY | ShiftMask, XK_n, togglealttag, {0}},
    TAGKEYS(XK_1, 0) TAGKEYS(XK_2, 1) TAGKEYS(XK_3, 2) TAGKEYS(XK_4, 3)
        TAGKEYS(XK_5, 4) TAGKEYS(XK_6, 5) TAGKEYS(XK_7, 6) TAGKEYS(XK_8, 7)
            TAGKEYS(XK_9, 8){MODKEY, XK_0, view, {.ui = ~0}},
    {MODKEY | ShiftMask, XK_0, tag, {.ui = ~0}},
    {MODKEY, XK_minus, spawn,
     SHCMD("pamixer --allow-boost -d 5; kill -44 $(pidof dwmblocks)")},
    // SHCMD("changeVolume 2dB- unmute; kill -44 $(pidof dwmblocks)")},
    // //changeVolume https://wiki.archlinux.org/index.php/Dunst
    {MODKEY | ShiftMask, XK_minus, spawn,
     SHCMD("pamixer --allow-boost -d 15; kill -44 $(pidof dwmblocks)")},
    {MODKEY, XK_equal, spawn,
     SHCMD("pamixer --allow-boost -i 5; kill -44 $(pidof dwmblocks)")},
    // SHCMD("changeVolume 2dB+ unmute; kill -44 $(pidof dwmblocks)")},
    // //changeVolume https://wiki.archlinux.org/index.php/Dunst
    {MODKEY | ShiftMask, XK_equal, spawn,
     SHCMD("pamixer --allow-boost -i 15; kill -44 $(pidof dwmblocks)")},
    {MODKEY, XK_BackSpace, spawn, SHCMD("sysact")},
    {MODKEY, XK_Tab, view, {0}},
    {MODKEY, XK_q, killclient, {0}},
    {MODKEY | ShiftMask, XK_q, spawn, SHCMD("sysact")},
    {ALT, XK_q, spawn, SHCMD("xdotool click 1")},
    {ALT, XK_w, spawn, SHCMD("switch")},
    {ALT, XK_e, spawn, SHCMD("xdotool click 3")},
    {MODKEY, XK_w, spawn, SHCMD("brave-bin")},
    {MODKEY | ShiftMask, XK_w, spawn, SHCMD("dmenusearch")},
    {ALT, XK_f, spawn, SHCMD("pcmanfm")},
    {ALT, XK_p, spawn, SHCMD("passmenu-otp")},
    {MODKEY, XK_e, spawn, SHCMD("doom everywhere")},
    //{MODKEY | ShiftMask, XK_r, spawn, SHCMD(TERMINAL " -e htop")},
    {MODKEY, XK_t, setlayout, {.v = &layouts[0]}},             /* tile */
    {MODKEY | ShiftMask, XK_t, setlayout, {.v = &layouts[1]}}, /* bstack */
    {MODKEY, XK_y, setlayout, {.v = &layouts[2]}},             /* spiral */
    {MODKEY | ShiftMask, XK_y, setlayout, {.v = &layouts[3]}}, /* dwindle */
    {MODKEY, XK_u, setlayout, {.v = &layouts[4]}},             /* deck */
    {MODKEY | ShiftMask, XK_u, setlayout, {.v = &layouts[5]}}, /* monocle */
    {MODKEY, XK_i, setlayout, {.v = &layouts[6]}}, /* centeredmaster */
    {MODKEY | ShiftMask,
     XK_i,
     setlayout,
     {.v = &layouts[7]}}, /* centeredfloatingmaster */
    {MODKEY | ShiftMask, XK_h, setlayout, {.v = &layouts[10]}},
    {MODKEY, XK_o, incnmaster, {.i = +1}},
    {MODKEY | ControlMask, XK_o, winview, {0}},
    {MODKEY | ShiftMask, XK_o, incnmaster, {.i = -1}},
    {MODKEY, XK_p, spawn, SHCMD("mpc toggle")},
    {MODKEY | ShiftMask, XK_p, spawn,
     SHCMD("pdf=$(locate -b .pdf .epub | dmenu -p 'Which book: '  -l 20) && [ "
           "-e $pdf ] && zathura $pdf")},
    {MODKEY, XK_bracketleft, spawn, SHCMD("mpc seek -10")},
    {MODKEY | ShiftMask, XK_bracketleft, spawn, SHCMD("mpc seek -60")},
    {MODKEY, XK_bracketright, spawn, SHCMD("mpc seek +10")},
    {MODKEY | ShiftMask, XK_bracketright, spawn, SHCMD("mpc seek +60")},
    {MODKEY, XK_backslash, view, {0}},

    {MODKEY, XK_a, togglegaps, {0}},
    {MODKEY | ShiftMask, XK_a, defaultgaps, {0}},
    {MODKEY, XK_s, togglesticky, {0}},
    {MODKEY, XK_d, spawn, {.v = dmenucmd}},
    {MODKEY, XK_f, togglefullscr, {0}},
    {MODKEY | ShiftMask, XK_f, setlayout, {.v = &layouts[8]}},
    //{MODKEY, XK_g, shiftview, {.i = -j}},
    {MODKEY, XK_g, setlayout, {.v = &layouts[3]}},
    {MODKEY | ShiftMask, XK_g, shifttag, {.i = -1}},
    {MODKEY, XK_h, setmfact, {.f = -0.05}},
    /* J and K are automatically bound above in STACKEYS */
    {MODKEY, XK_l, setmfact, {.f = +0.05}},
    //{ MODKEY,			semicolon_XK,	shiftview,	{ .i = 1 } },
    {MODKEY | ShiftMask, XK_semicolon, shifttag, {.i = 1}},
    {MODKEY, XK_apostrophe, togglescratch, {.ui = 1}},
    {ALT, XK_t, togglescratch, {.ui = 2}},
    //{MODKEY, XK_Return, spawn, {.v = termcmd}},
    {MODKEY, XK_Return, spawn, SHCMD("st")},
    {MODKEY | ShiftMask, XK_Return, togglescratch, {.ui = 0}},

    {MODKEY, XK_z, incrgaps, {.i = +3}},
    {MODKEY, XK_x, incrgaps, {.i = -3}},
    /* { MODKEY|ShiftMask,		XK_x,		spawn, SHCMD("")
     * },
     */
    /* { MODKEY,			XK_c,		spawn, SHCMD("")
     * },
     */
    /* { MODKEY|ShiftMask,		XK_c,		spawn, SHCMD("")
     * },
     */
    /* V is automatically bound above in STACKKEYS */
    {MODKEY, XK_b, togglebar, {0}},
    {MODKEY | ShiftMask, XK_b, spawn, SHCMD("setbg ~/Pictures/Wallpaper/")},

    // {MODKEY, XK_n, spawn, SHCMD(TERMINAL " -e nvim -c VimwikiIndex")},
    {MODKEY, XK_n, spawn,
     SHCMD(TERMINAL " -e newsboat; pkill -RTMIN+6 dwmblocks")},
    {MODKEY, XK_m, spawn, SHCMD(TERMINAL " -e ncmpcpp")},
    //{ALT, XK_m, , SHCMD("st -e ")},
    {ALT | ShiftMask, XK_l, spawn, SHCMD("/opt/listen1/listen1.AppImage")},
    {ALT | ShiftMask, XK_i, spawn, SHCMD("information")},
    {MODKEY | ShiftMask, XK_m, spawn,
     SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)")},
    {MODKEY, XK_comma, spawn, SHCMD("mpc prev")},
    {MODKEY | ShiftMask, XK_comma, spawn, SHCMD("mpc seek 0%")},
    {MODKEY, XK_period, spawn, SHCMD("mpc next")},
    {MODKEY | ShiftMask, XK_period, spawn, SHCMD("mpc repeat")},

    //{MODKEY, XK_Left, focusmon, {.i = -1}},
    //{MODKEY | ShiftMask, XK_Left, tagmon, {.i = -1}},
    //{MODKEY, XK_Right, focusmon, {.i = +1}},
    //{MODKEY | ShiftMask, XK_Right, tagmon, {.i = +1}},

    {MODKEY, XK_Page_Up, shiftview, {.i = -1}},
    {MODKEY | ShiftMask, XK_Page_Up, shifttag, {.i = -1}},
    {MODKEY, XK_Page_Down, shiftview, {.i = +1}},
    {MODKEY | ShiftMask, XK_Page_Down, shifttag, {.i = +1}},
    {ALT, XK_h, spawn, SHCMD("hidew")},
    {ALT | ShiftMask, XK_h, spawn, SHCMD("showw")},
    {MODKEY, XK_Insert, spawn,
     SHCMD("xdotool type $(cat ~/.local/share/larbs/snippets | dmenu -i -l 50 "
           "| cut -d' ' -f1)")},

    {MODKEY, XK_F1, spawn, SHCMD("st -e htop")},
    /* {MODKEY, XK_F2, spawn, SHCMD("mpdmenu")}, */
    {MODKEY, XK_F3, spawn, SHCMD("displayselect")},
    {MODKEY, XK_F4, spawn,
     SHCMD(TERMINAL " -e pulsemixer; kill -44 $(pidof dwmblocks)")},
    {MODKEY, XK_F5, xrdb, {.v = NULL}},
    //{MODKEY, XK_F6, spawn, SHCMD("torwrap")},
    {MODKEY, XK_F6, spawn,
     SHCMD("ifinstalled tremc-git transmission-cli || exit; ! pidof "
           "transmission-daemon >/dev/null && transmission-daemon && "
           "notify-send 'Starting torrent daemon...';$TERMINAL -e tremc; pkill "
           "-RTMIN+7 ${STATUSBAR:-dwmblocks}")},
    {MODKEY, XK_F7, spawn, SHCMD("td-toggle")},
    {MODKEY, XK_F9, spawn, SHCMD("dmenumount")},
    {MODKEY, XK_F10, spawn, SHCMD("dmenuumount")},
    {MODKEY, XK_F11, spawn,
     SHCMD("mpv --no-cache --no-osc --no-input-default-bindings "
           "--input-conf=/dev/null --title=webcam $(ls /dev/video[0,2,4,6,8] | "
           "tail -n 1)")},
//    {MODKEY, XK_F12, xrdb, {.v = NULL}},
	{ MODKEY,			XK_F12,		spawn,		SHCMD("remaps & notify-send \\\"⌨️ Keyboard remapping...\\\" \\\"Re-running keyboard defaults for any newly plugged-in keyboards.\\\"") },
    {MODKEY, XK_space, zoom, {0}},
    {MODKEY | ShiftMask, XK_space, togglefloating, {0}},

    {0, XK_Print, spawn, SHCMD("maim pic-full-$(date '+%y%m%d-%H%M-%S').png")},
    {ShiftMask, XK_Print, spawn, SHCMD("maimpick")},
    {MODKEY, XK_Print, spawn, SHCMD("dmenurecord")},
    {MODKEY | ShiftMask, XK_Print, spawn, SHCMD("dmenurecord kill")},
    {MODKEY, XK_Delete, spawn, SHCMD("dmenurecord kill")},
    {MODKEY, XK_Scroll_Lock, spawn, SHCMD("killall screenkey || screenkey &")},

    {0, XF86XK_AudioMute, spawn,
     SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)")},
    {0, XF86XK_AudioRaiseVolume, spawn,
     SHCMD("pamixer --allow-boost -i 3; kill -44 $(pidof dwmblocks)")},
    {0, XF86XK_AudioLowerVolume, spawn,
     SHCMD("pamixer --allow-boost -d 3; kill -44 $(pidof dwmblocks)")},
    {0, XF86XK_AudioPrev, spawn, SHCMD("mpc prev")},
    {0, XF86XK_AudioNext, spawn, SHCMD("mpc next")},
    {0, XF86XK_AudioPause, spawn, SHCMD("mpc pause")},
    {0, XF86XK_AudioPlay, spawn, SHCMD("mpc play")},
    {0, XF86XK_AudioStop, spawn, SHCMD("mpc stop")},
    {0, XF86XK_AudioRewind, spawn, SHCMD("mpc seek -10")},
    {0, XF86XK_AudioForward, spawn, SHCMD("mpc seek +10")},
    {0, XF86XK_AudioMedia, spawn, SHCMD(TERMINAL " -e ncmpcpp")},
    {0, XF86XK_AudioMicMute, spawn,
     SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle")},
    {0, XF86XK_PowerOff, spawn, SHCMD("sysact")},
    {0, XF86XK_Calculator, spawn, SHCMD(TERMINAL " -e bc -l")},
    {0, XF86XK_Sleep, spawn, SHCMD("sudo -A zzz")},
    {0, XF86XK_WWW, spawn, SHCMD("$BROWSER")},
    {0, XF86XK_DOS, spawn, SHCMD(TERMINAL)},
    {0, XF86XK_ScreenSaver, spawn,
     SHCMD("slock & xset dpms force off; mpc pause; pauseallmpv")},
    {0, XF86XK_TaskPane, spawn, SHCMD(TERMINAL " -e htop")},
    {0, XF86XK_Mail, spawn,
     SHCMD(TERMINAL " -e neomutt ; pkill -RTMIN+12 dwmblocks")},
    {0, XF86XK_MyComputer, spawn, SHCMD(TERMINAL " -e lf /")},
    /* { 0, XF86XK_Battery,		spawn,		SHCMD("") }, */
    {0, XF86XK_Launch1, spawn, SHCMD("xset dpms force off")},
    {0, XF86XK_TouchpadToggle, spawn,
     SHCMD("(synclient | grep 'TouchpadOff.*1' && synclient TouchpadOff=0) || "
           "synclient TouchpadOff=1")},
    {0, XF86XK_TouchpadOff, spawn, SHCMD("synclient TouchpadOff=1")},
    {0, XF86XK_TouchpadOn, spawn, SHCMD("synclient TouchpadOff=0")},
    {0, XF86XK_MonBrightnessUp, spawn, SHCMD("xbacklight -inc 15")},
    {0, XF86XK_MonBrightnessDown, spawn, SHCMD("xbacklight -dec 15")},

    /* { MODKEY|Mod4Mask,              XK_h,      incrgaps,       {.i = +1 } },
     */
    /* { MODKEY|Mod4Mask,              XK_l,      incrgaps,       {.i = -1 } },
     */
    /* { MODKEY|Mod4Mask|ShiftMask,    XK_h,      incrogaps,      {.i = +1 } },
     */
    /* { MODKEY|Mod4Mask|ShiftMask,    XK_l,      incrogaps,      {.i = -1 } },
     */
    /* { MODKEY|Mod4Mask|ControlMask,  XK_h,      incrigaps,      {.i = +1 } },
     */
    /* { MODKEY|Mod4Mask|ControlMask,  XK_l,      incrigaps,      {.i = -1 } },
     */
    /* { MODKEY|Mod4Mask|ShiftMask,    XK_0,      defaultgaps,    {0} }, */
    /* { MODKEY,                       XK_y,      incrihgaps,     {.i = +1 } },
     */
    /* { MODKEY,                       XK_o,      incrihgaps,     {.i = -1 } },
     */
    /* { MODKEY|ControlMask,           XK_y,      incrivgaps,     {.i = +1 } },
     */
    /* { MODKEY|ControlMask,           XK_o,      incrivgaps,     {.i = -1 } },
     */
    /* { MODKEY|Mod4Mask,              XK_y,      incrohgaps,     {.i = +1 } },
     */
    /* { MODKEY|Mod4Mask,              XK_o,      incrohgaps,     {.i = -1 } },
     */
    /* { MODKEY|ShiftMask,             XK_y,      incrovgaps,     {.i = +1 } },
     */
    /* { MODKEY|ShiftMask,             XK_o,      incrovgaps,     {.i = -1 } },
     */

};

/* resizemousescroll direction argument list */
static const int scrollargs[][2] = {
    /* width change         height change */
    {+scrollsensetivity, 0},
    {-scrollsensetivity, 0},
    {0, +scrollsensetivity},
    {0, -scrollsensetivity},
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle,
 * ClkClientWin, or ClkRootWin */
static Button buttons[] = {
/* click                event mask      button          function        argument
 */
#ifndef __OpenBSD__
    {ClkWinTitle, 0, Button2, zoom, {0}},
    {ClkClientWin, MODKEY, Button4, resizemousescroll, {.v = &scrollargs[0]}},
    {ClkClientWin, MODKEY, Button5, resizemousescroll, {.v = &scrollargs[1]}},
    {ClkClientWin, MODKEY, Button6, resizemousescroll, {.v = &scrollargs[2]}},
    {ClkClientWin, MODKEY, Button7, resizemousescroll, {.v = &scrollargs[3]}},
    {ClkStatusText, 0, Button1, sigdwmblocks, {.i = 1}},
    {ClkStatusText, 0, Button2, sigdwmblocks, {.i = 2}},
    {ClkStatusText, 0, Button3, sigdwmblocks, {.i = 3}},
    {ClkStatusText, 0, Button4, sigdwmblocks, {.i = 4}},
    {ClkStatusText, 0, Button5, sigdwmblocks, {.i = 5}},
    {ClkStatusText, ShiftMask, Button1, sigdwmblocks, {.i = 6}},
#endif
    {ClkStatusText, ShiftMask, Button3, spawn, SHCMD("launcher")},
    {ClkClientWin, MODKEY, Button1, movemouse, {0}},
    {ClkClientWin, MODKEY, Button2, defaultgaps, {0}},
    {ClkClientWin, MODKEY, Button3, resizemouse, {0}},
    {ClkClientWin, MODKEY, Button4, resizemousescroll, {.v = &scrollargs[0]}},
    {ClkClientWin, MODKEY, Button5, resizemousescroll, {.v = &scrollargs[1]}},
    {ClkClientWin, MODKEY, Button6, resizemousescroll, {.v = &scrollargs[2]}},
    {ClkClientWin, MODKEY, Button7, resizemousescroll, {.v = &scrollargs[3]}},
    //{ ClkClientWin,		MODKEY,		Button4,	incrgaps,
    //{.i = +1}
    //}, { ClkClientWin,		MODKEY,		Button5,
    // incrgaps,	{.i = -1} },
    {ClkTagBar, 0, Button1, view, {0}},
    {ClkTagBar, 0, Button3, toggleview, {0}},
    {ClkTagBar, MODKEY, Button1, tag, {0}},
    {ClkTagBar, MODKEY, Button3, toggletag, {0}},
    {ClkTagBar, 0, Button4, shiftview, {.i = -1}},
    {ClkTagBar, 0, Button5, shiftview, {.i = 1}},
    {ClkRootWin, MODKEY, Button2, togglebar, {0}},
    //{ClkRootWin, 0, Button3, spawn, SHCMD("jgmenu_run")},
};
static const char *ipcsockpath = "/tmp/dwm.sock";
static IPCCommand ipccommands[] = {
    IPCCOMMAND(view, 1, {ARG_TYPE_UINT}),
    IPCCOMMAND(toggleview, 1, {ARG_TYPE_UINT}),
    IPCCOMMAND(tag, 1, {ARG_TYPE_UINT}),
    IPCCOMMAND(toggletag, 1, {ARG_TYPE_UINT}),
    IPCCOMMAND(tagmon, 1, {ARG_TYPE_UINT}),
    IPCCOMMAND(focusmon, 1, {ARG_TYPE_SINT}),
    IPCCOMMAND(focusstack, 1, {ARG_TYPE_SINT}),
    IPCCOMMAND(zoom, 1, {ARG_TYPE_NONE}),
    IPCCOMMAND(incnmaster, 1, {ARG_TYPE_SINT}),
    IPCCOMMAND(killclient, 1, {ARG_TYPE_SINT}),
    IPCCOMMAND(togglefloating, 1, {ARG_TYPE_NONE}),
    IPCCOMMAND(setmfact, 1, {ARG_TYPE_FLOAT}),
    IPCCOMMAND(setlayoutsafe, 1, {ARG_TYPE_PTR}),
    IPCCOMMAND(quit, 1, {ARG_TYPE_NONE})};

/* signal definitions */
/* signum must be greater than 0 */
/* trigger signals using `xsetroot -name "fsignal:<signum>"` */
static Signal signals[] = {
    /* signum       function        argument  */
    {1, setlayout, {.v = 0}},
};

